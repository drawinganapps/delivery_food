import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_delivery/helper/color_helper.dart';
import 'package:food_delivery/sources/dummy_data.dart';
import 'package:food_delivery/widgets/filter_widget.dart';
import 'package:food_delivery/widgets/item_card_widget.dart';
import 'package:food_delivery/widgets/search_widget.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return ListView(
      physics: const BouncingScrollPhysics(),
      children: [
        Container(
          margin: EdgeInsets.only(top: heightSize * 0.03),
          padding:
              EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          child: const SearchWidget(),
        ),
        SizedBox(
          height: heightSize * 0.1,
          child: ListView(
            physics: const BouncingScrollPhysics(),
            scrollDirection: Axis.horizontal,
            children: List.generate(DummyData.categories.length, (index) {
              return GestureDetector(
                onTap: () {},
                child: FilterWidget(
                    isSelected: index == 1,
                    filter: DummyData.categories[index]),
              );
            }),
          ),
        ),
        Container(
            margin: EdgeInsets.only(bottom: heightSize * 0.03),
            padding: EdgeInsets.only(
                left: widthSize * 0.05, right: widthSize * 0.05),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Recommended Food',
                    style: TextStyle(
                        color: ColorHelper.white,
                        fontSize: 16,
                        fontWeight: FontWeight.w600)),
                Text('See all',
                    style: TextStyle(
                        color: ColorHelper.grey,
                        fontSize: 14,
                        fontWeight: FontWeight.w600))
              ],
            )),
        Container(
            padding: EdgeInsets.only(
                left: widthSize * 0.05, right: widthSize * 0.05),
            child: GridView.count(
              shrinkWrap: true,
              crossAxisCount: 2,
              crossAxisSpacing: 15,
              mainAxisSpacing: 15,
              childAspectRatio: 0.75,
              physics: const BouncingScrollPhysics(),
              children: List.generate(DummyData.items.length, (index) {
                return ItemCardWidget(
                  item: DummyData.items[index],
                );
              }),
            ))
      ],
    );
  }
}
