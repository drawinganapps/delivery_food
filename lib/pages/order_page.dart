import 'package:flutter/material.dart';
import 'package:food_delivery/helper/color_helper.dart';
import 'package:food_delivery/sources/dummy_data.dart';
import 'package:food_delivery/widgets/apply_coupon_widget.dart';
import 'package:food_delivery/widgets/order_item_card_widget.dart';

class OrderPage extends StatelessWidget {
  const OrderPage({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return ListView(
      physics: const BouncingScrollPhysics(),
      children: [
        Container(
          height: heightSize * 0.48,
          margin: EdgeInsets.only(top: heightSize * 0.01),
          padding:
              EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          child: ListView(
            padding: const EdgeInsets.all(0),
            physics: const BouncingScrollPhysics(),
            children: List.generate(DummyData.items.length - 1, (index) {
              return Container(
                margin: EdgeInsets.only(bottom: heightSize * 0.025),
                child: OrderItemCardWidget(item: DummyData.items[index]),
              );
            }),
          ),
        ),
        Container(
          padding:
              EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(bottom: heightSize * 0.03),
                padding: EdgeInsets.only(
                    left: widthSize * 0.08, right: widthSize * 0.08),
                child: Container(
                    decoration: BoxDecoration(
                        border: Border(
                            top: BorderSide(
                                color: ColorHelper.grey.withOpacity(0.4))))),
              ),
              const ApplyCouponWidget()
            ],
          ),
        ),
        Container(
          padding:
              EdgeInsets.only(left: widthSize * 0.08, right: widthSize * 0.08),
          margin: EdgeInsets.only(
              top: heightSize * 0.03, bottom: heightSize * 0.03),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Total',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: ColorHelper.yellow)),
              Text('\$ 26.80',
                  style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.w600,
                      color: ColorHelper.yellow)),
            ],
          ),
        ),
        Container(
          padding:
              EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          child: Container(
              height: heightSize * 0.08,
              decoration: BoxDecoration(
                  color: ColorHelper.yellow,
                  borderRadius: BorderRadius.circular(20)),
              child: Center(
                child: Text('Proceed to Checkout',
                    style: TextStyle(
                        color: ColorHelper.white,
                        fontSize: 22,
                        fontWeight: FontWeight.w600)),
              )),
        ),
      ],
    );
  }
}
