import 'package:flutter/material.dart';
import 'package:food_delivery/helper/color_helper.dart';

class ItemDetailActionWidget extends StatelessWidget {

  const ItemDetailActionWidget({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Container(
      padding: EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05, bottom: heightSize * 0.03, top: heightSize * 0.01),
      height: heightSize * 0.12,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Container(
                padding: const EdgeInsets.all(3),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: ColorHelper.tertiary),
                child:
                Icon(Icons.remove, color: ColorHelper.quaternary),
              ),
              Container(
                margin: EdgeInsets.only(
                    left: widthSize * 0.035, right: widthSize * 0.035),
                child: Text('1',
                    style: TextStyle(
                        fontWeight: FontWeight.w900,
                        fontSize: 25,
                        color: ColorHelper.white)),
              ),
              Container(
                padding: const EdgeInsets.all(3),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: ColorHelper.quaternary),
                child: Icon(Icons.add, color: ColorHelper.yellow),
              ),
            ],
          ),
          Container(
            width: widthSize * 0.5,
            padding: EdgeInsets.only(
                top: heightSize * 0.025,
                bottom: heightSize * 0.025,
                left: widthSize * 0.08,
                right: widthSize * 0.08),
            decoration: BoxDecoration(
                color: ColorHelper.yellow,
                borderRadius: BorderRadius.circular(10)),
            child: Center(
              child: Text('Add to Cart',
                  style: TextStyle(
                      color: ColorHelper.white,
                      fontSize: 18,
                      fontWeight: FontWeight.w600)),
            ),
          ),
        ],
      ),
    );
  }
  
}