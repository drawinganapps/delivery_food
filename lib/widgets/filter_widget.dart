import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_delivery/helper/color_helper.dart';
import 'package:food_delivery/models/category_model.dart';

class FilterWidget extends StatelessWidget {
  final bool isSelected;
  final CategoryModel filter;

  const FilterWidget({Key? key, required this.isSelected, required this.filter})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    return Center(
      child: Container(
        margin: EdgeInsets.only(left: screenWidth * 0.05),
        child: Container(
          padding: EdgeInsets.only(
              left: screenHeight * 0.02,
              right: screenHeight * 0.02,
              top: screenHeight * 0.01,
              bottom: screenHeight * 0.01),
          decoration: BoxDecoration(
              color: isSelected ? ColorHelper.quaternary : ColorHelper.secondary,
              borderRadius: BorderRadius.circular(20)),
          child: Row(
            children: [
             Container(
               margin: EdgeInsets.only(right: screenWidth * 0.02),
               child: Image.asset('assets/images/${filter.icon}', height: 20, width: 20),
             ),
              Text(filter.name,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 14,
                      color: ColorHelper.yellow)),
            ],
          ),
        ),
      ),
    );

  }
}
