import 'package:flutter/material.dart';
import 'package:food_delivery/helper/color_helper.dart';
import 'package:food_delivery/routes/AppRoutes.dart';
import 'package:go_router/go_router.dart';

class NavigationWidget extends StatelessWidget {
  final int selectedMenu;

  const NavigationWidget({super.key, required this.selectedMenu});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Container(
      height: heightSize * 0.09,
      padding: EdgeInsets.only(
        left: widthSize * 0.03,
        right: widthSize * 0.03,
      ),
      decoration: BoxDecoration(
          border:
              Border(top: BorderSide(color: ColorHelper.tertiary, width: 2))),
      child: Container(
        decoration: BoxDecoration(
            color: ColorHelper.primary,
            borderRadius: BorderRadius.circular(35)),
        padding:
            EdgeInsets.only(left: widthSize * 0.03, right: widthSize * 0.03),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
           GestureDetector(
             child:  Container(
               decoration: const BoxDecoration(shape: BoxShape.circle),
               padding: EdgeInsets.all(heightSize * 0.015),
               child: Icon(Icons.home_rounded,
                   size: selectedMenu == 0 ? 45 : 35,
                   color: selectedMenu == 0
                       ? ColorHelper.yellow
                       : ColorHelper.yellow.withOpacity(0.3)),
             ),
             onTap: () => context.go(AppRoutes.HOME),
           ),
            Container(
              decoration: const BoxDecoration(shape: BoxShape.circle),
              padding: EdgeInsets.all(heightSize * 0.015),
              child: Icon(Icons.notifications_rounded,
                  size: selectedMenu == 1 ? 45 : 35,
                  color: selectedMenu == 1
                      ? ColorHelper.yellow
                      : ColorHelper.yellow.withOpacity(0.3)),
            ),
            GestureDetector(
              child: Container(
                decoration: const BoxDecoration(shape: BoxShape.circle),
                padding: EdgeInsets.all(heightSize * 0.015),
                child: Icon(Icons.wallet_outlined,
                    size: selectedMenu == 2 ? 45 : 35,
                    color: selectedMenu == 2
                        ? ColorHelper.yellow
                        : ColorHelper.yellow.withOpacity(0.3)),
              ),
              onTap: () => context.go(AppRoutes.ORDERS),
            ),
            Container(
              decoration: const BoxDecoration(shape: BoxShape.circle),
              padding: EdgeInsets.all(heightSize * 0.015),
              child: Icon(Icons.sms_rounded,
                  size: selectedMenu == 3 ? 45 : 35,
                  color: selectedMenu == 3
                      ? ColorHelper.yellow
                      : ColorHelper.yellow.withOpacity(0.3)),
            ),
            Container(
              decoration: const BoxDecoration(shape: BoxShape.circle),
              padding: EdgeInsets.all(heightSize * 0.015),
              child: Icon(Icons.person_rounded,
                  size: selectedMenu == 4 ? 45 : 35,
                  color: selectedMenu == 4
                      ? ColorHelper.yellow
                      : ColorHelper.yellow.withOpacity(0.3)),
            ),
          ],
        ),
      ),
    );
  }
}
