import 'package:flutter/material.dart';
import 'package:food_delivery/helper/color_helper.dart';
import 'package:food_delivery/models/item_model.dart';

class OrderItemCardWidget extends StatelessWidget {
  final ItemModel item;

  const OrderItemCardWidget({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Container(
      height: heightSize * 0.12,
      padding: EdgeInsets.only(left: widthSize * 0.03, right: widthSize * 0.03),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(25),
          color: ColorHelper.secondary),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset('assets/images/${item.icon}', fit: BoxFit.cover),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text(item.name,
                  style: TextStyle(color: ColorHelper.white, fontSize: 16)),
              Row(
                children: [
                  Container(
                    padding: const EdgeInsets.all(3),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: ColorHelper.tertiary),
                    child: Icon(Icons.remove, color: ColorHelper.quaternary),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        left: widthSize * 0.035, right: widthSize * 0.035),
                    child: Text('1',
                        style: TextStyle(
                            fontWeight: FontWeight.w900,
                            fontSize: 25,
                            color: ColorHelper.white)),
                  ),
                  Container(
                    padding: const EdgeInsets.all(3),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: ColorHelper.quaternary),
                    child: Icon(Icons.add, color: ColorHelper.yellow),
                  ),
                ],
              ),
            ],
          ),
          Text('\$ ${item.price}',
              style: TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.w600,
                  color: ColorHelper.yellow))
        ],
      ),
    );
  }
}
