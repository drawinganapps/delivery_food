import 'package:flutter/material.dart';
import 'package:food_delivery/helper/color_helper.dart';

class ApplyCouponWidget extends StatelessWidget {
  const ApplyCouponWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 70,
      child: TextField(
        decoration: InputDecoration(
            contentPadding:
                const EdgeInsets.only(left: 15, right: 15, top: 20, bottom: 20),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20),
                borderSide: BorderSide(color: ColorHelper.primary, width: 0)),
            filled: true,
            fillColor: ColorHelper.tertiary,
            hintText: 'Add promo Code',
            suffixIcon: Container(
              width: 90,
              margin: const EdgeInsets.only(right: 20),
              padding: const EdgeInsets.only(top: 12, bottom: 12),
              child: Container(
                decoration: BoxDecoration(
                    color: ColorHelper.quaternary,
                    borderRadius: BorderRadius.circular(20)),
                child: Center(
                  child: Text('Apply',
                      style: TextStyle(
                          color: ColorHelper.yellow,
                          fontWeight: FontWeight.w600)),
                ),
              ),
            ),
            hintStyle: TextStyle(
                fontWeight: FontWeight.w500, color: ColorHelper.grey)),
      ),
    );
  }
}
