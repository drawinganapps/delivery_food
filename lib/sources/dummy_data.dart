import 'package:food_delivery/models/category_model.dart';
import 'package:food_delivery/models/item_model.dart';

class DummyData {
  static List<CategoryModel> categories = [
    CategoryModel(1, 'All', 'all.png'),
    CategoryModel(2, 'Breakfast', 'breakfast.png'),
    CategoryModel(3, 'Takoyaki', 'takoyaki.png'),
    CategoryModel(4, 'Snack', 'snack.png'),
  ];

  static List<ItemModel> items = [
    ItemModel(1, 'Ramen', 'ramen.png', 8.2, 4.6),
    ItemModel(1, 'Miso Nugget', 'miso.png', 6.7, 4.5),
    ItemModel(1, 'Salmon Sushi', 'sushi.png', 11.9, 4.7),
    ItemModel(1, 'Katsu', 'katsu.png', 9.9, 4.3),
  ];

  static String description = """You boil some water while you desperately tear open the packet. The water bubbles away madly as you reach to cut the taste maker within the two-minute noodle packet.
  \nTwo minutes, maybe three minutes later, you’re burning your tongue as you wallop down the noodles. What you’ve experienced is an instant result.
  And readers too want to experience that very same instant result with your content. They don’t want to waffle through mountains of information. They want a nice, quick bite to begin with. Of course, it helps if you have the right parameters in place.
  """;
}