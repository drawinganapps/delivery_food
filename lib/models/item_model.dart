class ItemModel {
  int id;
  String name;
  String icon;
  double price;
  double rate;

  ItemModel(this.id, this.name, this.icon, this.price, this.rate);
}
