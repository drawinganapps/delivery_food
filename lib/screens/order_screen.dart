import 'package:flutter/material.dart';
import 'package:food_delivery/helper/color_helper.dart';
import 'package:food_delivery/pages/order_page.dart';
import 'package:food_delivery/routes/AppRoutes.dart';
import 'package:food_delivery/widgets/navigation_widget.dart';
import 'package:go_router/go_router.dart';

class OrderScreen extends StatelessWidget {
  const OrderScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorHelper.primary,
        title: Text('My Bag',
            style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: ColorHelper.white)),
        elevation: 0,
        leadingWidth: widthSize * 0.16,
        leading: Center(
          child: GestureDetector(
            child: Container(
              padding: const EdgeInsets.all(10),
              margin: EdgeInsets.only(left: widthSize * 0.05),
              decoration: BoxDecoration(
                  color: ColorHelper.tertiary,
                  borderRadius: BorderRadius.circular(10)),
              child: Icon(Icons.arrow_back_rounded, color: ColorHelper.white),
            ),
            onTap: () => context.go(AppRoutes.HOME),
          ),
        ),
        actions: [
          Container(
            margin: EdgeInsets.only(right: widthSize * 0.05),
            child: Center(
              child: Text('3 items',
                  style: TextStyle(
                      color: ColorHelper.white.withOpacity(0.8),
                      fontSize: 18,
                      fontWeight: FontWeight.w200)),
            ),
          )
        ],
      ),
      body: const OrderPage(),
      bottomNavigationBar: const NavigationWidget(selectedMenu: 2),
    );
  }
}
