import 'package:food_delivery/routes/AppRoutes.dart';
import 'package:food_delivery/screens/home_screen.dart';
import 'package:food_delivery/screens/item_detail_screen.dart';
import 'package:food_delivery/screens/order_screen.dart';
import 'package:go_router/go_router.dart';

class AppPage {
// GoRouter configuration
  static var routes = GoRouter(
    initialLocation: AppRoutes.HOME,
    routes: [
      GoRoute(
        path: AppRoutes.HOME,
        builder: (context, state) => const HomeScreen(),
      ),
      GoRoute(
        path: AppRoutes.DETAILS,
        builder: (context, state) => const ItemDetailScreen(),
      ),
      GoRoute(
        path: AppRoutes.ORDERS,
        builder: (context, state) => const OrderScreen(),
      ),
    ],
  );
}
