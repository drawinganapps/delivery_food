class AppRoutes {
  static const String HOME = '/home';
  static const String DETAILS = '/details';
  static const String ORDERS = '/orders';
}
